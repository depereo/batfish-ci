#!/usr/bin/env python3

from pybatfish.question.question import load_questions, list_questions
from pybatfish.client.commands import bf_session
from exitstatus import ExitStatus
import sys

test_passed = False

# check to see if batfish questions load properly
bf_session.host = 'localhost'
try:
    load_questions()
    test_passed = True if list_questions() else False
except:
    test_passed = False

if test_passed:
    sys.exit(ExitStatus.success)
else:
    sys.exit(ExitStatus.failure)
