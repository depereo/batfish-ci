FROM docker.io/batfish/batfish

# Fix for https://github.com/pypa/pip/issues/10219
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# set up python3
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    && pip3 install --upgrade pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/oracle*

# install python packages needed for my batfish tests
RUN pip3 install pybatfish ansible ipaddr exitstatus

# run the batfish server, but drop your session into bash
ENTRYPOINT /bin/bash -c " \
            java \
            -XX:-UseCompressedOops \
            -XX:+UseContainerSupport \
            -XX:MaxRAMPercentage=80 \
            -cp allinone-bundle.jar \
            org.batfish.allinone.Main \
            -runclient false \
            -loglevel warn \
            -coordinatorargs '-templatedirs questions -containerslocation /data/containers' &" && bash
