# Batfish CI Image

This is a gitlab-compatible batfish image that can be used in a gitlab-ci pipeline.

## Batfish

Batfish is a network emulation technology that simulates network state. It has a server-client
architecture, and is interacted with using pybatfish or other client technology.

## This Image

Batfish generally assumes a persistent server, which stores state longer-term and allows
network or systems adminstrators to monitor and manage batfish test success rates, for example.

My use-case however is for an infrastructure-as-code network, where I would like to run a ci pipeline
dynamically against feature branches. The workflow is therefore to generate proposed network
configurations, upload these as a 'snapshot' to batfish, and ask 'batfish questions' to determine
if my desired functionality will still be in place post-change.

Therefore, I need an image with an entrypoint that starts the server, but still permits interaction
to run ci tests inside the container.

This image takes the batfish-published image, installs ansible, pybatfish and other required
libraries, and can then run provided tests.

## Examples

1) Generate a batfish snapshot in some manner, I usually use ansible. Save the config snapshot as an `artifact`.

```
config_gen:
  image: python:3.8.3-buster
  stage: build
  script:
    # generate proposed network config
    - pip3 install ansible
    - exit_code_ansible=0
    - ansible-playbook playbooks/03-config_gen.yaml --skip-tags "config_push" || eval "exit_code_ansible=\$?"
    - eval "[[ ${exit_code_ansible} == 0 ]]"
  # save proposed network config snapshot
  # Will be available for other stages
  artifacts:
    name: "snapshot"
    untracked: true
    paths:
      - ./bf_snapshot
    expire_in: 1 days
```

2) Use this image to run a pytbatfish program!

```
policy_eval:
  # use image that provides a batfish server in pipeline
  image: registry.gitlab.com/depereo/batfish-ci:latest
  stage: test
  dependencies:
    - config_gen
  script:
    - exit_code_python=0
    - python3 tests/bf_bgp_test.py || eval "exit_code_python=\$?"
    - eval "[[ ${exit_code_python} == 0 ]]"
```



## Credits
* **Project** https://github.com/batfish/batfish

